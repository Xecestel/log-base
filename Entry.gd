class_name Entry

# Variables

var name : String;
var description : String;

###########

# Sets the entry name
func set_name(NAME : String) -> void:
	self.name = NAME;


# Returns the entry name
func get_name() -> String:
	return self.name;


# Sets the entry description
func set_description(DESCRIPTION : String) -> void:
	self.description = DESCRIPTION;


# Returns the entry description
func get_description() -> String:
	return self.description;
