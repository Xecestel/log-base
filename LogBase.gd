class_name Log

# Variables

var name : String;
var Entries : Dictionary;
var entries_number = 0;

###########

# Log constructor
func _init(NAME : String = "") -> void:
	self.set_name(NAME);
	
	
# Sets a name to the log
func set_name(NAME : String = "") -> void:
	self.name = NAME;


# Returns the name of the log
func get_name() -> String:
	return self.name;


# Returns the Entries Array
func get_entries() -> Dictionary:
	return self.Entries;


# Returns an entry given its key
func get_entry(key : String):
	return self.Entries.get(key);


# Adds a new entry to the log
func add_entry(entry) -> void:
	var key = entry.get_name();
	if (self.Entries.has(key) == false):
		self.Entries[key] = entry;
		self.entries_number += 1;


# Removes an entry from the log
func remove_entry(key : String) -> void:
	if (Entries.has(key)):
		self.Entries.erase(key);
		self.entries_number -= 1;


# Removes every entry from the log
func clear_entries() -> void:
	self.Entries.clear();


# Returns the total number of entries
func get_entries_number() -> int:
	return self.entries_number;
