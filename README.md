# Log Base
## Version 1.1
## © Xecestel
## Licensed Under MIT License

## Overview
This Log Base works as a base class for a videogame log, like a journal, or a quest log, or a bestiary.  
This repository contains two scripts:
- LogBase.gd, containing the methods and variables of the Log class (basically the log name and an array of entries);
- Entry.gd, which is the base class for a log entry (like a journal note, or a quest), basically an empty class with just a name and a description.

## Methods
### LogBase.gd
The LogBase.gd file contains the internal methods of the Log class:
- `_init(NAME : String = "") -> void`: this is the constructor. It only needs the log name, so you can make your own constructor for your custom subclass, managing the data storage as you wish;
- `get_entry(id : String)`: this method returns an Entry object from the Entries Array, given its index;
- `add_entry(entry) -> void`: this method adds an Entry object to the Entries Array if it's not already in there;
- `remove_entry(id : String) -> void`: this method removes an entry from the Entries Array given its index;
- `get_entries_number() -> int`: this method returns the number of the entries on the log;
- `set_name(NAME : String = "") -> void`: this is the setter for the log name;
- `get_name() -> String`: this is the getter for the log name;
- `get_entries() -> Array`: this returns the entire Entries Array of the log.
- `clear_entries() -> void`: clears all the Entries from the list.

### Entry.gd
The Entry.gd file contains the internal methods of the Entry class:
- `set_name(NAME : String) -> void`: this is the setter for the entry name;
- `get_name() -> String`: this is the getter for the entry name;
- `set_description(DESCRIPTION : String) -> void`: this is the setter for the entry description;
- `get_description() -> String`: this is the getter for the entry description

## Notes
If you want to see an example of usage for this class, you can take a check out the [Quest Log Class](https://gitlab.com/Xecestel/quest-log).  

This class was originally made as part of the [RPG Template project](https://gitlab.com/Xecestel/rpg-template).  

## Licenses
Copyright © 2019-2020 Celeste Privitera  
This software is an open source project licensed under MIT License. Check the LICENSE file for more info.  

## Changelog
### Version 1.0
- The class was created

### Version 1.1
- The Entries are now stored in a Dictionary to make the access easier
- Improved code redability
